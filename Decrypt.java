package org.codeforafrica.timby;

import java.io.File;

import javax.crypto.Cipher;

import org.codeforafrica.timby.Export2SDService.export2SD;
import org.codeforafrica.timby.media.Encryption;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class Decrypt extends Activity{
	public String filename;
	public int mode;

    public void onCreate() {
    super.onCreate();
	
	Bundle extras = intent.getExtras(); 
	filename = (String) extras.get("filepath");
	mode = Cipher.DECRYPT_MODE;	
	
	Cipher cipher;
	String file = filename;
	try {
		
		cipher = EncryptionModel.createCipher(mode);
		EncryptionModel.applyCipher(file, file+"_", cipher);
	}catch (Exception e) {
		// TODO Auto-generated catch block
		Log.e("Encryption error", e.getLocalizedMessage());
		e.printStackTrace();
	}
	//Then delete original file
	File oldfile = new File(file);
	oldfile.delete();
	
	//Then remove _ on encrypted file
	File newfile = new File(file+"_");
	newfile.renameTo(new File(file));
	}
}