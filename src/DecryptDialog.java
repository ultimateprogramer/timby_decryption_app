import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.swing.*;
import java.io.*;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class DecryptDialog extends javax.swing.JDialog {
    private javax.swing.JPanel contentPane;
    private javax.swing.JButton buttonDecrypt;
    private javax.swing.JButton buttonExit;

    InputStream is = null;
    OutputStream os = null;

    public DecryptDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonDecrypt);

        buttonDecrypt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                try {
                    onOK();
                } catch (InvalidKeyException e1) {
                    e1.printStackTrace();
                    dispose();
                } catch (NoSuchPaddingException e1) {
                    e1.printStackTrace();
                    dispose();
                } catch (NoSuchAlgorithmException e1) {
                    e1.printStackTrace();
                    dispose();
                } catch (FileNotFoundException e1) {
                    e1.printStackTrace();
                    dispose();
                }
            }
        });

        buttonExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                onCancel();
            }
        });

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent e) {
                onCancel();
            }
        });

        contentPane.registerKeyboardAction(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent e) {
                onCancel();
            }
        }, javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0), javax.swing.JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, FileNotFoundException {
        // Streams and streams initialization

        //Create a file chooser
        final JFileChooser fc = new JFileChooser();

        //In response to a button click:
        int returnVal = fc.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION)
        {
            File file = fc.getSelectedFile();
            int mode = Cipher.DECRYPT_MODE;
            Cipher cipher;
            String file_name = file.getPath();
            String new_file_name = file.getParentFile().getPath() + "/decrypted.zip";

            try
            {
                cipher = EncryptionModel.createCipher(mode);
                EncryptionModel.applyCipher(file_name , new_file_name, cipher);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }

            // Then delete original file
            File old_file = new File(file_name);
            old_file.delete();

            // Then remove _ on encrypted file
            File new_file = new File(new_file_name);
            new_file.renameTo(new File(file_name));
        }
    }

    private void onCancel() {
        dispose();
    }

    public static void main(String[] args) {
        DecryptDialog dialog = new DecryptDialog();
        dialog.pack();
        dialog.setVisible(true);
        System.exit(0);
    }
}
